<?php
// $Id$

/**
 * @file
 * smileys-plugin for wysiwyg module.
 */

function wysiwyg_smileys_plugin_smileys_plugin() {
  $plugins['smileys'] = array(
    'title' => t('Smileys'),
    'vendor url' => 'http://drupal.qpart.ru/',
    'icon file' => 'smileys.png',
    'icon title' => t('Smileys plugin'),
    'settings' => array(),
  );
  return $plugins;
}

